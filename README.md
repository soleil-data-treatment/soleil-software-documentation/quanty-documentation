# Aide et ressources de Quanty pour Synchrotron SOLEIL

[![](http://www.quanty.org/lib/tpl/vector/user/logo.gif)](http://www.quanty.org/start)

## Résumé
- Calculs atomiques, quantum many body
- Privé

## Sources
- Code source: Non, pas de distribution
- Documentation officielle: http://www.quanty.org/documentation/start

## Navigation rapide
| Ressources annexes | Page pan-data |
| ------ | ------ |
| [Forum officiel](http://www.quanty.org/forum/start) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/98/quanty) |

## Installation
- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien)

## Format de données
- en entrée: pas de format
- en sortie: pas de format
- sur la Ruche
